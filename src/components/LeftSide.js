import light from "../assets/images/light-img.png"
import dark from "../assets/images/dark-img.png"

function LeftSide(){
    return(
        <section className="left-side">
            <img src={light} alt="img" className="light"/>
            <img src={dark} alt="img" className="dark"/>
        </section>
    );
}

export default LeftSide