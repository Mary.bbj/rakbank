import google from "../assets/images/google.png"
import fb from "../assets/images/fb.png"
import apple from "../assets/images/apple.png"
 
function Socials(){
    return(
        <div className="socials">
            <a href="/"> <img src={google} alt="icon"/> Signin with Google</a>
            <a href="/"><img src={fb} alt="icon"/> </a>
            <a href="/"><img src={apple} alt="icon"/> </a>
        </div>
    );
}

export default Socials