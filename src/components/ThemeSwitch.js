import circle from "../assets/images/circle.png"

const LeftSide = props =>{
    return(
        <section  className="theme-switch">
            <div className="logo">
                <h1>Travelguru</h1>
                <img src={circle} alt="img"/>
            </div>
            <div>
                <label className="switch">
                    <input type="checkbox" checked={props.theme} onChange={e => props.onThemeChange(e.target.checked)}/>
                    <span className="slider"/>
                </label>
            </div>
        </section>
    );
}

export default LeftSide
