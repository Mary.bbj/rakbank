import LeftSide from "../components/LeftSide"
import ThemeSwitch from "../components/ThemeSwitch"
import Socials from "../components/Socials"
import { useState } from "react"

const Signin = props =>{
    const [darkTheme, setDarkTheme] = useState(false);

    return(
        <section className={`signin ${darkTheme ? 'dark' : ""}`}>
            <div className="wrapper">
                <LeftSide theme={darkTheme} onThemeChange={setDarkTheme}/>
                <section className="right-side">
                    <ThemeSwitch  theme={darkTheme} onThemeChange={setDarkTheme}/>
                    <div className="signup">
                        <h2>Sign in to Travelguru</h2>
                        <p>Don't have an account? <a href="#" >Sign up</a></p>
                    </div>
                    <form>
                        <input type="text" placeholder="Full name"/>
                        <input type="email" placeholder="Email"/>
                        <input type="password" placeholder="Password"/>
                        <button>Continue</button>
                    </form>
                    <p className="or">- OR -</p>
                    <Socials />
                </section>
            </div>
        </section>
    );
}

export default Signin
